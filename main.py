#!/usr/bin/env python3
"""icon-name with rendered icon"""
LINK = "https://developer.gnome.org/icon-naming-spec/"

import os, sys

__filepath__ = os.path.realpath(__file__)
PWD = os.path.dirname(__filepath__) + '/'

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk, GdkPixbuf
from urllib.request import urlopen
from html.parser import HTMLParser

class PageParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)

        self.n0 = None
        self.COUNT_table = -1
        self.FLAG_tbody  = False

        self.data = []
        self.treestore = Gtk.TreeStore(str, str, str)


    def feed(self, data):
        HTMLParser.feed(self, data)
        return self.treestore


    def handle_starttag(self, tag, attrs):
        if ('class', 'table-contents') in attrs: self.COUNT_table += 1
        if 'tbody' == tag: self.FLAG_tbody = True
        if 'table' == tag and self.COUNT_table:
            for a, v in attrs:
                if "summary" == a:
                    self.n0 = self.treestore.append(None, [None, v, None])


    def handle_endtag(self, tag):
        if tag == 'tbody': self.FLAG_tbody = False
        if tag == 'tr':
            if len(self.data) < 2: return
            name, *desc = self.data
            self.treestore.append(self.n0, [ name, name, ' '.join(desc) ])
            self.data.clear()


    def handle_data(self, data):
        if self.COUNT_table and self.FLAG_tbody:
            self.data.append(data.replace('\n', '').strip())


class Window(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title='Icon Naming Specification')
        self.set_default_size(600, 480)
        self.makeWidgets()
        self.connect('key_press_event', self.on_key_press)


    def makeWidgets(self):
        self.layout = Gtk.Box(orientation=1)
        self.add(self.layout)

        label = Gtk.Label()
        label.set_markup("{0}\n<a href='{1}'>{1}</a>".format(__doc__, LINK))
        self.layout.pack_start(label, expand=False, fill=False, padding=5)

        self.notebook = Gtk.Notebook()
        self.layout.pack_start(self.notebook, expand=True, fill=True, padding=0)
        self.notebook.append_page(self.makeWidget_STOCK(), Gtk.Label(label="Gtk.STOCK"))
        self.notebook.append_page(self.makeWidget_theme(), Gtk.Label(label="Gtk.IconTheme"))


    def makeWidget_treeview(self, treestore):
        scroll = Gtk.ScrolledWindow()
        scroll.set_hexpand(True)
        scroll.set_vexpand(True)

        self.treeview = Gtk.TreeView(model=treestore)
        scroll.add(self.treeview)
        self.treeview.append_column(Gtk.TreeViewColumn("image", Gtk.CellRendererPixbuf(), icon_name=0))
        self.treeview.append_column(Gtk.TreeViewColumn("icon name", Gtk.CellRendererText(), text=1))
        self.treeview.append_column(Gtk.TreeViewColumn("description", Gtk.CellRendererText(), text=2))
        return scroll


    def makeWidget_STOCK(self):
        liststore = Gtk.ListStore(str, str, str)
        for item in dir(Gtk):
            if "STOCK" in item:
                name = "Gtk."+item
                attr = getattr(Gtk, item)
                liststore.append([attr, name, attr])

        return self.makeWidget_treeview(liststore)


    def makeWidget_theme(self):
        liststore = Gtk.ListStore(str, str, str)
        icon_theme = Gtk.IconTheme.get_default()
        for item in icon_theme.list_icons():
            liststore.append([item, item, None])

        return self.makeWidget_treeview(liststore)


    def on_key_press(self, widget, event):
        if event.keyval == 65307:  Gtk.main_quit()
        elif Gdk.ModifierType.MOD1_MASK & event.state:
            if ord('1') <= event.keyval <= ord('9'):
                self.notebook.set_current_page(event.keyval - ord('1'))


def main():
    win = Window()
    FILE = PWD + 'index.html'
    if os.path.isfile(FILE):
        html_page = open(FILE).read()
    else:
        print("fetaching:", LINK)
        obj = urlopen(LINK)
        if obj.msg != 'OK': return win
        html_page = obj.read().decode('utf-8')
        open(FILE, 'w').write(html_page)

    parser = PageParser()
    treestore = parser.feed(html_page)
    win.notebook.append_page(win.makeWidget_treeview(treestore), Gtk.Label(label="icon-name"))
    return win


if __name__ == '__main__':
    root = main()
    root.show_all()
    root.connect('delete-event', Gtk.main_quit)
    root.connect( # quit when Esc is pressed
        'key_release_event',
        lambda w, e: Gtk.main_quit() if e.keyval == 65307 else None
    )

    Gtk.main()
